import {useState, useEffect, Fragment} from 'react';
import Orders from './Order.js';
import Table from 'react-bootstrap/Table';

export default function Order({userOrder}) {
	const [orders, setOrders] = useState([])

	useEffect( () => {
		const orderArr = userOrder.map( order => {
			
			if(order !== null){
				return(
					<Orders 
						orderProp={userOrder} 
						key={order._id}
					/>
				)
			} else {
				return null
			}
		})

		setOrders(orderArr)

	}, [userOrder])


	return(
		<Fragment>
			<h1 className="text-center py-3">Order History</h1>
			<Table className="mx-5">
				{orders}
			</Table>
		</Fragment>
	)
}
