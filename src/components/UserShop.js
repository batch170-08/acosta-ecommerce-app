import {useState, useEffect, Fragment} from 'react';
import Products from './Product.js';
import Row from 'react-bootstrap/Row';

export default function UserShop({product}) {
	const [products, setProducts] = useState([])

	useEffect( () => {
		const productArr = product.map( product => {
			
			if(product.isAvailable === true){
				return(
					<Products 
						productProp={product} 
						key={product._id}
					/>
				)
			} else {
				return null
			}
		})

		setProducts(productArr)

	}, [product])


	return(
		<Fragment>
			<h1 className="text-center py-3">Travel Without Moving. Just Build It!</h1>
			<Row className="mx-5">
				{products}
			</Row>
		</Fragment>
	)
}
