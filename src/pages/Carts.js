import React, { useState } from 'react';
import { FaTrashAlt } from "react-icons/fa"
import {Container, Button, Row} from 'react-bootstrap';

export default function Carts(props) {

	const {
    name,
    description,
    isAvailable,
    currency,
    image,
    stock,
    price,
    id,
    changeCart } = props;

	const { products, setProducts } = useState([])

	const deleteFromCart = () => {
		const filtered = products.filter(product => product.id !== id)
		setProducts({products: filtered})
		localStorage.setItem('products', JSON.stringify(filtered))
	}

	const emptyCart = () => {
		setProducts({products: []})
		localStorage.setItem('products', JSON.stringify([]))
	}

	const result = products.map(product => {
			return (
				<Container>
					<Row>
						<img className="img_product" src={product.image} alt={product.name} />
						<h5>{product.name}</h5>
						<p>₱{product.price}</p>
						<Button onClick={deleteFromCart(product.id)}><FaTrashAlt style={{color: 'red'}}/></Button>
					</Row>
				</Container>
			)
		}
	)	


	const prices = products.map(product => {
		return +product.price
	})

	const totalAmount = prices.reduce((acc, curr) => acc + curr, 0)

	return (
		<Container>
			<h1>Order Summary</h1>
			<Row>
				{result.length ?  result : <li> No product in your cart </li>}
			</Row>
			<h3>${totalAmount}</h3>
			<Button variant="danger" onClick={emptyCart}>Empty Cart</Button>
		</Container>
	)
}
