import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
/*import './index.css';*/
import UserContext from './userContext.js';
import AppNavbar from './components/AppNavbar.js'
import Home from './pages/Home.js';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Orders from './pages/Orders';
import Carts from './pages/Carts';



export default function App() {
	const [ user, setUser ] = useState( {
		access: localStorage.getItem('access'),
		isAdmin: localStorage.getItem('isAdmin') === true,
		products: localStorage.getItem('products')
	 });

	const unsetUser = () => {
		localStorage.clear();
		setUser( {
			access: null,
			isAdmin: null
		} )
	}
	
	return (
		<UserContext.Provider value={{ user, setUser, unsetUser }}>
			 <Router>
			   <AppNavbar user={user} />
			   <Routes>
			       	<Route path = "/" element={<Home />} />
					<Route path = "/register" element = {<Register />} />
			       	<Route path = "/login" element = {<Login />} />
			       	<Route path="*" element = {<Error />} />
			       	<Route path = "/products" element ={<Products />} />
			       	<Route path = "/orders" element ={<Orders />} />
			       	<Route path = "/cart" element ={<Carts />} />
			   </Routes>
			</Router>
		</UserContext.Provider>
	)
}	
