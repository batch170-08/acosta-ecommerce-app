import React from 'react';
import { Row,Col,Image } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner( { data } ) {

    const {title, content, destination, label} = data;

    return (
        <Row className="mt-5">
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}
