import {Fragment, useEffect, useState, useContext} from 'react'
import UserContext from '../userContext'
import Order from './../components/Order';
import AllOrders from "./../components/AllOrders";
import UserOrder from "./../components/UserOrder";

export default function Orders(){
	
	const [orders, setOrders] = useState([])
	const {user} = useContext(UserContext)

	const fetchData = () => {
		fetch("https://glacial-lowlands-08229.herokuapp.com/ecommerce/orders/")
		.then(response => response.json())
		.then(data => {
			setOrders(data)
		})
	}
	
	useEffect(() => {
		fetchData()
	}, [])

	return (
		<Fragment>
			{
				(user.isAdmin === true ) ?
					<AllOrders order= {orders} fetchData={fetchData}/>
				:
					<UserOrder order= {orders}/>
			}
		</Fragment>
	)
}




/*const [orders, setOrders] = useState([])
	const {user} = useContext(UserContext)

	const fetchData = (e) => {
		fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/users/profile`,
		    headers: {"Authorization": `Bearer ${data.access}`}
		)
		.then(response => response.json())
		.then(data => {
			if (data.isAdmin === true) {
				fetch('https://glacial-lowlands-08229.herokuapp.com/ecommerce/orders', 
					headers: {"Authorization": `Bearer ${data.access}`}
				)
				setOrders(data)
			} else {
				fetch('https://glacial-lowlands-08229.herokuapp.com/ecommerce/orders/my-orders', 
					headers: {"Authorization": `Bearer ${data.access}`}
				)
				setOrders(data)
			}
		})

	useEffect(() => {
		fetchData()
	}, [])

	return (

	)*/