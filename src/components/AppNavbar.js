import React, { Fragment, useContext } from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../userContext.js'
import { FaShoppingCart } from "react-icons/fa"
import Container from 'react-bootstrap/Container';

export default function AppNavbar() {
	const { user, unsetUser } = useContext(UserContext);
	const navigate = useNavigate();

	const logout = () => {
		unsetUser();
		navigate('/login');
	}

	let rightNav = (user.access === null) ? (

		<Fragment>
			<Nav.Link as={NavLink} to='/cart'>
				<h6><FaShoppingCart style={{color: 'yellow'}}/></h6>
			</Nav.Link>
			<Nav.Link as={NavLink} to='/register'>Register</Nav.Link>
			<Nav.Link as={NavLink} to='/login'>Login</Nav.Link>
		</Fragment>	
		) : (
		<Fragment>
			<Nav.Link as={NavLink} to='/orders'>Orders</Nav.Link>
			<Nav.Link onClick={logout}>Logout</Nav.Link>
			<Nav.Link as={NavLink} to='/cart'>
				<h6><FaShoppingCart style={{color: 'yellow'}}/></h6>
			</Nav.Link>
		</Fragment>	
		)

	return (
			<Navbar className="navbar-dark m-2" bg="info" expand='lg'>
			    <Navbar.Brand as={Link} to='/'>BiMi!</Navbar.Brand>
			    <Navbar.Toggle aria-controls="basic-navbar-nav" />
			    <Navbar.Collapse id="basic-navbar-nav">
			        <Nav className="mr-auto">
			            <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
			            <Nav.Link as={NavLink} to='/products'>Products</Nav.Link>
			        </Nav>
			        <Nav className = 'ml-auto'>
			          {rightNav}
			        </Nav>
			    </Navbar.Collapse>
			</Navbar>
	)
}