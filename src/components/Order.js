import {useState, useEffect, Fragment} from 'react';
import Orders from './Order.js';
import Table from 'react-bootstrap/Table';

export default function Order({userOrder}) {
  const [orders, setOrders] = useState([])

  const {
    name,
    description,
    isAvailable,
    currency,
    image,
    stock,
    price,
    _id} = userOrder;


  return(
        <Table striped bordered hover>
          {orders}
        </Table>
  )
}



/*import {useState,useEffect} from 'react'
import {Container, Row, Col, Card, Table, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


export default function Orders({props}) {

  const { order, fetchData } = props;

  const [orderId, setOrderId] = useState('');
  const [purchasedOn, setPurchasedOn] = useState('');
  const [totalAmount, setTotalAmount] = useState('');
  const [itemsOrdered, setItemsOrdered] = useState([]);
  const [price, setPrice] = useState('');
  const [quantity, setquantity] = useState('');
  const [ orders, setOrders ] = useState([]);

  useEffect(() => {
    const orderArr = order.map(order => {
      return (
        <tbody>
          <tr>{order.id} + {order.purchasedOn}</tr>
          <tr>{order.itemsOrdered}</tr>
          <tr>{order.totalAmount}</tr>
        </tbody>
         
      )
    })
    setOrders(orderArr)
  }, [order])


  const {
    totalAmount,
    purchasedOn,
    itemsOrdered,
    _id} = orderProp;

   let navigate = useNavigate()
    const route = () => {
      let path = `/orders`
      navigate(path)
    }

  return (
      <Container>
        <h1>Order History</h1>
        <Table striped bordered hover>
            {order}
        </Table>
      </Container>
  )
}





import React from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


export default function Orders({orderProp}) {

  const { order, fetchData } = props;

  const [orderId, setOrderId] = useState('');
  const [purchasedOn, setPurchasedOn] = useState('');
  const [totalAmount, setTotalAmount] = useState('');
  const [itemsOrdered, setItemsOrdered] = useState([]);
  const [price, setPrice] = useState('');
  const [quantity, setquantity] = useState('');
  const [ orders, setOrders ] = useState([]);

  useEffect(() => {
    const orderArr = order.map(order => {
      return (
         <tr>{order.id} + {order.purchasedOn}</tr>
         <tr>{order.itemsOrdered}</tr>
         <tr>{order.totalAmount}</tr>
      )
    })
    setOrders(orderArr)
  }, [order])


    const {
    totalAmount,
    purchasedOn,
    itemsOrdered,
    _id} = orderProp;

  let navigate = useNavigate()
    const route = () => {
      let path = `/orders`
      navigate(path)
    }

  return (
      <Container>
        <Table striped bordered hover>
          <tbody>
            {order}
          </tbody>
        </Table>
      </Container>
  )
}*/