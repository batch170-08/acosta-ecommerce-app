import {Fragment, useEffect, useState, useContext} from 'react'
import UserContext from '../userContext'
import Product from './../components/Product';
import AdminDashboard from "./../components/AdminDashboard";
import UserShop from "./../components/UserShop";

export default function Products(){

	const [products, setProducts] = useState([])
	const {user} = useContext(UserContext)

	const fetchData = () => {
		fetch("https://glacial-lowlands-08229.herokuapp.com/ecommerce/products/")
		.then(response => response.json())
		.then(data => {
			setProducts(data)
		})
	}
	
	useEffect(() => {
		fetchData()
	}, [])

	return (
		<Fragment>
			{
				(user.isAdmin === true ) ?
					<AdminDashboard product= {products} fetchData={fetchData}/>
				:
					<UserShop product= {products}/>
			}
		</Fragment>
	)
}
