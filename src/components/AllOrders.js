import {useState,useEffect} from 'react'
import {Container, Button, Row, Col, Table} from 'react-bootstrap';

export default function AllOrders(props) {

	const { order, fetchData } = props;

	const [orderId, setOrderId] = useState('');
	const [purchasedOn, setPurchasedOn] = useState('');
	const [totalAmount, setTotalAmount] = useState('');
	const [itemsOrdered, setItemsOrdered] = useState([]);
	const [price, setPrice] = useState('');
	const [quantity, setquantity] = useState('');
	const [ orders, setOrders ] = useState([]);

	useEffect(() => {
		const orderArr = order.map(order => {
			return (
				<tbody>
					<tr>{order.id} + {order.purchasedOn}</tr>
					<tr>{order.itemsOrdered}</tr>
					<tr>{order.totalAmount}</tr>
				</tbody>
				 
			)
		})
		setOrders(orderArr)
	}, [order])

	return (
		<Container>
			<Table striped bordered hover>
				{order}
			</Table>
		</Container>
	)
}
