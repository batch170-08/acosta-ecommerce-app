import {useState, Fragment, useEffect} from 'react'
import {Container, Button, Row, Col, Table, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminDashboard(props) {

	const { product, fetchData } = props;

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [stock, setStock] = useState('');
	const [image, setImage] = useState('');
	const [productId, setProductId] = useState('');

	const [products, setProducts] = useState([]);

	const [showAdd, setShowAdd] = useState(false)

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)


	useEffect(() => {
		const productArr = product.map(product => {
			return (
				<tr>
				 <td>{product.name}</td>
				 <td>{product.description}</td>
				 <td>{product.price}</td>
				 <td>
				 	{(product.stock > 0) ? 
				 		<span>{product.stock + " Available"}</span>
				 		:
				 		<span>Out of stock</span>}

				 </td>
				 <td>
					<Button variant="primary" onClick={() => openEdit(product._id)}>Update</Button>
					{
						(product.isAvailable) ?
							<Fragment>
								<Button
								className= "my-2"
								variant="danger"
								onClick={() => archiveProduct(product._id, product.isAvailable)}>
									Disable
								</Button>
							</Fragment>
							:
							<Fragment>
								<Button variant="success">Enable</Button>
							</Fragment>
						}
				 	</td>	  
				</tr>
			)
		})
		setProducts(productArr)
	}, [product])

	const addProduct = (e) => {
		e.preventDefault()

		fetch("https://glacial-lowlands-08229.herokuapp.com/ecommerce/products/add", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					headers: {"Authorization": `Bearer ${localStorage.getItem("access")}`}
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					stock: stock,
					image: image
				})
			})
			.then(response => response.json())
			.then(data => {
				if(data === true) {
					Swal.fire({
				    title: "We're all set!",
				    icon: "success",
				    text: "Product is successfully added."
				})
					setName('')
					setDescription('')
					setPrice('')
					setStock('')
					setImage('')
					closeAdd()
					
			} else {
				Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."	
				})
				setName('')
				setDescription('')
				setPrice('')
				setStock('')
				setImage('')
				closeAdd()
				fetchData()
			}
		})
	}

	const openEdit = (productId) => {

		fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/products/${productId}`, {
			method: "GET",
			headers: {"Authorization": `Bearer ${localStorage.getItem("access")}`}
		})
		.then(response => response.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
			setImage(data.price)
		})
		setShowAdd(true)
	}

	const editProduct = (e, productId) => {
		e.preventDefault()

		fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/products/${productId}/update`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("access")}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					stock: stock,
					image: image
				})
			})
			.then(response => response.json())
			.then(data => {
				if (typeof data !== undefined) {
					fetchData()
					Swal.fire({
				    title: "All set!",
				    icon: "success",
				    text: "Product is successfully update."
				})
					closeAdd()
				} else {
					Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."	
				})
				fetchData()		
			}
		})
	}

	const archiveProduct = (productId, isAvailable) => {

		fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/products/${productId}/archive`, {
				method: "PUT",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem("access")}`
				},
				body: JSON.stringify({
					isAvailable: !isAvailable
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data === true) {
				fetchData()
				Swal.fire({
				    title: "All set!",
				    icon: "success",
				    text: "Product disabled."
				})
			} else {
				fetchData()
					Swal.fire({
					title: "Something went wrong.",
					icon: "error",
					text: "Please try again."	
				})
			}
		})
	}

	return (
		<Container>
			<h2>Admin Dashboard</h2>
			<Row>
				<Col>
					<Button onClick={openAdd}>Add Product</Button>
				</Col>
			</Row>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>

		<Modal show={showAdd} onHide={closeAdd}>
			<Modal.Header closeButton>
				<Modal.Title>Add Product</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form onSubmit={ (e) => addProduct(e)}>
					<Form.Group controlId="productName">
						<Form.Label>Product Name:</Form.Label>
						<Form.Control 
							type="text"
							value={name}
							onChange={ (e) => setName(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="productDescription">
						<Form.Label>Description:</Form.Label>
						<Form.Control 
							type="text"
							value={description}
							onChange={ (e) => setDescription(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="productPrice">
						<Form.Label>Price:</Form.Label>
						<Form.Control 
							type="text"
							value={price}
							onChange={ (e) => setPrice(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="productStock">
						<Form.Label>Stock:</Form.Label>
						<Form.Control 
							type="text"
							value={stock}
							onChange={ (e) => setStock(e.target.value)}/>
					</Form.Group>
					<Form.Group controlId="productImage">
						<Form.Label>Image Link:</Form.Label>
						<Form.Control 
							type="text"
							value={image}
							onChange={ (e) => setImage(e.target.value)}/>
					</Form.Group>
					<Button variant="success" type="submit" onSubmit={ (e) => addProduct(e)}>
					    Submit
					</Button>
					<Button 
						variant="secondary" 
						type="submit" 
						onClick={closeAdd}>
					    Close
					</Button>
				</Form>
			</Modal.Body>
		</Modal>
		</Container>
	)
}