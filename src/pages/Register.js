import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../userContext'
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Register(){

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ passwordConfirm, setPasswordConfirm ] = useState('');
	const [ isDisabled, setIsDisabled ] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isMobileNotEmpty = mobileNo !== '';
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;

		if ( isFirstNameNotEmpty && isLastNameNotEmpty && isMobileNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ firstName, lastName, mobileNo, email, password, passwordConfirm ]);

	function register(e){
		e.preventDefault();

		fetch('https://glacial-lowlands-08229.herokuapp.com/ecommerce/users/check', {
			method:"POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
            	email: email
            })
		})
		.then(response => response.json())
		.then(data => {
			if(data === false) {
				fetch('https://glacial-lowlands-08229.herokuapp.com/ecommerce/users/register', {
						method:"POST",
						headers: {"Content-Type": "application/json"},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password,
							passwordConfirm: passwordConfirm
					})
				})
				.then(response => response.json())
				.then(data => {
					if (data === true) {
						setUser({access: data.access})
						Swal.fire({
						    title: "We're all set!",
						    icon: "success",
						    text: "Let's have fun!"
						})
						setFirstName('')
						setLastName('')
						setMobileNo('')
						setEmail('')
						setPassword('')
						setPasswordConfirm('')
						navigate("/products");
					} else {
						alert(data.error)
					}
				})
			} else {
				Swal.fire("Duplicate email found, you can try logging in.")
				navigate("/login");
			}
		})
	}

	if (user.access !== null) {
		return navigate('/products')
	}

	return(
			<Container >
				<h2>Take your first step in building!</h2>
				<Form onSubmit={register}>
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="tel" placeholder="Enter your mobile number" value={mobileNo} pattern="[0-9]{11}" onChange={(e) => setMobileNo(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm password</Form.Label>
						<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
					</Form.Group>
					<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
		)
}