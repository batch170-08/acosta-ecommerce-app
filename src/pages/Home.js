import React, { Fragment } from 'react';
import Banner from '../components/Banner.js';
import Highlight from '../components/Highlight.js';



export default function Home() {

  const data = {
      title: "Build it, Make it!",
      content: "Explore the world of endless possibilities. Just build it!",
      destination: "/products",
      label: "Let's go!"
  }

  return (
    <Fragment>
      <Banner data={data}/>
      <Highlight />
    </Fragment>
  )
}