import React from 'react';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import { useNavigate } from 'react-router-dom';


export default function Highlight() {

	  let navigate = useNavigate()
	  const route = () => {
	    let path = `/products`
	    navigate(path)
	  }

	return (
	<Container fluid>
		<Row>
			<Col xs={12} md={6} >
					<Card className="border-0">
					  <Card.Img variant="top" src="https://www.lego.com/cdn/cs/set/assets/bltfd3e8e59d950ac57/21044.jpg" />
					  <Card.Body>
					    <Card.Title>Paris</Card.Title>
					    <Card.Text>
					      Bring together iconic Paris landmarks with this magnificent Paris skyline model. 
					    </Card.Text>
					    <Button variant="primary" onClick={route}>View more</Button>
					  </Card.Body>
					</Card>
			</Col>
			<Col xs={12} md={6}>
					<Card className="border-0">
					  <Card.Img variant="top" src="https://www.lego.com/cdn/cs/set/assets/blt0b2057b44f21d3bb/21051.jpg?fit=bounds&format=jpg&quality=80&width=1500&height=1500&dpr=1" />
					  <Card.Body>
					    <Card.Title>Tokyo</Card.Title>
					    <Card.Text>
					      Recreate some of Tokyo’s most famous buildings.
					    </Card.Text>
					    <Button variant="primary" onClick={route}>View more</Button>
					  </Card.Body>
					</Card>
			</Col>
		</Row>
		<Row className="mt-3">
			<Col xs={{span:12, order:'last'}} md={{span:8, order:'first'}} className="p-0 mt-5">
				<Image className="h-100 w-75 p-5 mx-5" src="https://www.lego.com/cdn/cs/set/assets/blt86423e4ec25d4312/10276.jpg?fit=bounds&format=jpg&quality=80&width=1500&height=1500&dpr=1" />
			</Col>
			<Col xs={{span:12, order:'first'}} md={{span:4, order:'first'}}  className="pt-5 mt-5">
				<Card className="border-0 mx-auto">
				  <Card.Body className="text-center">
							<Card.Title>
								<h1>When in Rome!</h1>
							</Card.Title>
							<Card.Text>
								<h4>One of the New 7 Wonders. Once the largest amphitheater in the world. Now the largest LEGO® set ever.</h4>
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>
)}