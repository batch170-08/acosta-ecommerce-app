import React, { useState, useEffect, useContext } from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Products({productProp}) {

  const {
    name,
    description,
    isAvailable,
    currency,
    image,
    stock,
    price,
    id,
    changeCart } = productProp;

    const [ isAdded, setIsAdded ] = useState('false');
    

    let navigate = useNavigate()
    /*const route = () => {
      let path = `/orders`
      navigate(path)
    }*/

   
    const addToCart = () => {
      const products = localStorage.setItem(products)
      if (!isAdded) {
        products.push({id, name, description, price, image})
        localStorage.setItem("products", JSON.stringify(products));
        setIsAdded(true)
        changeCart(products.length)
      } else {
        const products = localStorage.setItem(products)
        const newProducts = products.filter((item) => item.id !== id);
        localStorage.setItem("products", JSON.stringify(newProducts));
        setIsAdded(false)
        changeCart(newProducts.length);
      }
    }


  return (
      <Col xs={12} md={6} lg={4} className='p-4'>
        <Card className="border-0 px-2 py-5">
          <Card.Img fluid variant="top" src={image} alt="" />
          <Card.Body className="">
            <Card.Title className="pt-5 font-weight-bold">{name}</Card.Title>
            <Card.Text>₱{price}</Card.Text>
            <Button variant="primary" onClick={addToCart}>Add to Cart</Button>
          </Card.Body>
        </Card>
      </Col>
    )
}



/*import React, { useState } from 'react';
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import {Button} from 'react-bootstrap';


export default function Products(props) {

	return (
      <Col xs={12} md={6} lg={4} className='p-4'>
        <Card className="border-0 px-2 py-5">
          <Card.Img fluid variant="top" src={props.image} alt="" />
          <Card.Body className="">
            <Card.Title className="pt-5 font-weight-bold">{props.name}</Card.Title>
            <Card.Text><span className="mx-1">{props.currency}</span>{props.price}</Card.Text>
            <Card.Text>{props.stock + " Available"}</Card.Text>
            <Button variant="primary">Add to Cart</Button>
          </Card.Body>
        </Card>
      </Col>
    )
}
*/


 /*<Row>
      <Col className="m-auto" xs={12} md={6}>
           <Card.Img className="p-3 m-5 h-100 w-100" variant="top" src={product.image} />
      </Col>
      <Col xs={12} md={6}>
        <Card className="border-0 p-3 m-5">
          <Card.Body>
            <Card.Title>{product.name}</Card.Title>
            <Card.Text id="cardText" className="text-justify" >{product.description}</Card.Text>
            <h5>Price</h5>
            <Card.Text><span className="mx-1">{product.currency}</span>{product.price}</Card.Text>
            <Card.Text>{product.stock + " Available"}</Card.Text>
            <Button variant="primary">View</Button>
            <Button variant="danger" className="mx-3" onClick={() =>
                props.addToCart({
                  id: product.name,
                  product,
                  amount: 1
                })
            }
            >Add to Cart</Button>
          </Card.Body>
        </Card>    
      </Col>
    </Row>*/