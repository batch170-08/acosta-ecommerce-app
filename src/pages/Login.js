import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import UserContext from '../userContext'
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Login() {

    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [ isDisabled, setIsDisabled ] = useState(true);

    useEffect(()=>{
        let isEmailNotEmpty = email !== '';
        let isPasswordNotEmpty = password !== '';

        if ( isEmailNotEmpty && isPasswordNotEmpty ) {
            setIsDisabled(false);
        } else {
            setIsDisabled(true);
        }
        },[ email, password ]);

    function login(e) {
        e.preventDefault();

        fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/users/login`, {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(data => {
            if (data.access !== undefined) {
                localStorage.setItem('access', data.access)
                setUser({access: data.access})
                
                fetch(`https://glacial-lowlands-08229.herokuapp.com/ecommerce/users/profile`, {
                    headers: {"Authorization": `Bearer ${data.access}`}
                })
                .then(response => response.json())
                .then(data => {
                    if (data.isAdmin === true) {
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({isAdmin: data.isAdmin})
                        Swal.fire({
                            title: "Hello Admin",
                            icon: "success",
                            text: "Let's have fun!"
                        })
                    } else {
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({isAdmin: data.isAdmin})
                        Swal.fire({
                            title: "Login Successful",
                            icon: "success",
                            text: "Let's have fun!"
                        })
                    }
                })
            } else {
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Try again."
                })       
            }
        })
    }

    if (user.access !== null) {
        return navigate('/')
    }

    return (
        <Container fluid>
            <h3>Login</h3>
            <Form onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
            </Form>
        </Container>
    )
}